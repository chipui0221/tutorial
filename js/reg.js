
var header=["username","email","password"]
var UP_ARROW="&#x2191;"
var DOWN_ARROW="&#x2193;"
var UP_DOWN_ARROW="&#x2195;"

var regListRef=firebase.database().ref("users/")



regListRef.on("value",function(snapshot){
    loadList(snapshot)
})


console.log(config.databaseURL)

function createXHR(reqType,url){
    var xhr=new XMLHttpRequest()
    xhr.open(reqType,url)
    xhr.setRequestHeader('Content-Type','json')
    return xhr
}


function loadList(snapshot){
    var table=document.getElementById("regList")
      
    table.innerHTML=""
    
    var tr=document.createElement("tr")
    for(var i=0;i<header.length;i++){
        var th=document.createElement("th")
        var s=""
        if(header[i]=="Name"||header[i]=="Age"){
            s=header[i]+UP_DOWN_ARROW
            th.setAttribute("name",header[i])
            th.setAttribute("onclick","sortColumn(this)")
        }
        else{
            s=header[i]
        }
        th.innerHTML=s
        tr.appendChild(th)
    }
    table.appendChild(tr)
    
    
    snapshot.forEach(function(record){
        var tr=document.createElement("tr")
        for(var j=0;j<header.length;j++){
            var td=document.createElement("td")
            td.innerHTML=record.val()[header[j]]
            tr.appendChild(td)
        }
        table.appendChild(tr)
    })
    

}


function newReg(){
    var username = document.getElementById("username").value
    var password = document.getElementById("password").value
    var cf_password = document.getElementById("confirm_password").value
    var email = document.getElementById("email").value
   // var gender = document.getElementById("gender").value
    if(cf_password!=password){
        alert("confirm password incorrect");
    }else if(hasUser(username)){
         alert(username="the username has been used")
    }else{
        var user={
            "username":username,
            "name":username,
            "email":email,
            "password":password
        }
    
      var xhr=createXHR('POST',config.databaseURL+'/users.json')
      xhr.send(JSON.stringify(user))
    }
}



function getAgeAvg(){
    var totalAge=0
    var count=0
    regListRef.once('value',function(snapshot){
        snapshot.forEach(function(record){
            totalAge+=record.val()["Age"]
            count++
        })
    })


    var avgAge=totalAge/count
    alert("Average Age:"+avgAge)

}


function hasUser(username){


    var result=false
    regListRef.orderByChild("username").equalTo(username).on("child_added",function(snapshot){
        result=true
    })
    return result
}

function sortColumn(th){
    var column=th.getAttribute("name")
    var tempRef=regListRef.orderByChild(column)
    tempRef.once('value',function(snapshot){
        loadList(snapshot)
    })
    console.log(column+" sorted")
    //loadList(regList)
}

function filterAge(){
    var minAge=parseInt(document.getElementById("minAge").value)
    var maxAge=parseInt(document.getElementById("maxAge").value)
    var tempRef=regListRef
    console.log(minAge+" "+maxAge)
    if(!isNaN(minAge) && !isNaN(maxAge)){
        tempRef=tempRef.orderByChild("Age").startAt(minAge).endAt(maxAge)
    }
    tempRef.once('value',function(snapshot){
        loadList(snapshot)
    })
}




